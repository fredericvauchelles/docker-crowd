#!/bin/bash
set -e

mkdir -p "$CONF_HOME";
chmod 700 "$CONF_HOME";
chown -R crowd: "$CONF_HOME";

exec gosu crowd "${CONF_INSTALL}/apache-tomcat/bin/catalina.sh" "run" "$@";
